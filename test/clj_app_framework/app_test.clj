(ns clj-app-framework.app-test
  (:require [clj-app-framework.app :as app]
            [clojure.test :refer [deftest is testing]]))

(defn sample-task [{:keys [success]}]
  (if success
    ["YEAH!" nil]
    [nil "NO!"]))

(def registry-map
  {:dostuff {:func sample-task}
   :doother #(throw (Exception. %))})

(defn system-fn [_]
  (fn [task-fn]
    (task-fn
     {:success true})))

(def exec-input {:registry-map registry-map
                 :system-fn system-fn})

(deftest test-cli-exec
  (testing "Simple cli-test"
    (is (= (app/exec {:log-level :trace
                      :action :dostuff}
                     exec-input)
           ["YEAH!" nil]))
    (is (nil? (first (app/exec {:log-level :debug
                                :action :doother}
                               exec-input))))))